import ESProxyService from '../ESProxyService';

// eslint-disable-next-line no-undef
describe('ESProxyService', () => {
  // eslint-disable-next-line no-undef
  test('Gets a document from elasticsearch', (done) => {
    const indexName = 'chembl_molecule';
    const docID = 'CHEMBL59';
    const docSource = [
      'molecule_structures.canonical_smiles',
      '_metadata.drug.drug_data.applicants',
    ];
    ESProxyService.getESDocument(indexName, docID, docSource)
      .then((response) => {
        const dataReceived = response.data._source;
        const dataMustBe = {
          _metadata: {
            drug: {
              drug_data: {
                applicants: [
                  'American Regent Inc',
                  'Teligent Ou',
                  'Warner Chilcott Div Warner Lambert Co',
                  'Lyphomed Div Fujisawa Usa Inc',
                  'Baxter Healthcare Corp Anesthesia And Critical Care',
                  'Abbott Laboratories Hosp Products Div',
                  'International Medication System',
                  'Teva Parenteral Medicines Inc',
                  'B Braun Medical Inc',
                  'Abraxis Pharmaceutical Products',
                ],
              },
            },
          },
          molecule_structures: {
            canonical_smiles: 'NCCc1ccc(O)c(O)c1',
          },
        };

        // eslint-disable-next-line no-undef
        expect(dataReceived).toEqual(dataMustBe)
        done()
      })
      .catch((error) => {
        done(error)
      });
  });
});
