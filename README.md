# ChEMBL Elasticsearch Service

The NPM Repository is here: https://www.npmjs.com/package/@chembl/chembl-elasticsearch-service

This package can be used in web applications to load data from the ChEMBL's elasticsearch.

# Examples

## Getting a single document:

To load this:
```
GET chembl_molecule/_doc/CHEMBL59?_source=molecule_structures.canonical_smiles,_metadata.drug.drug_data.applicants
```

Use the following code:

```
ESProxyService.getESDocument(indexName, docID, docSource)
  .then((response) => {
    const dataReceived = response.data._source;
    // Do something with the data
  })
  .catch((error) => {
    // handle error
  });
```
