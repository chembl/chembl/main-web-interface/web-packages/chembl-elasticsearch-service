import axios from 'axios';

const esProxyApiClient = axios.create({
  // eslint-disable-next-line no-undef
  baseURL: process.env.esProxyBaseUrl,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
  },
});

export { esProxyApiClient };
