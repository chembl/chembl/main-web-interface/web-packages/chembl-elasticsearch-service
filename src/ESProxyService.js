import { esProxyApiClient } from './ESProxyAPIClient.js';

const methods = {
  // this is necessary to build URLs on the server side
  addQueryParamsToURL(baseURL, params) {
    const stringParams = [];
    Object.entries(params).forEach(([key, value]) => {
      let strValue = '';
      if (Array.isArray(value)) {
        strValue = value.join(',');
      } else {
        strValue = String(value);
      }
      stringParams.push(`${key}=${encodeURIComponent(strValue)}`);
    });
    if (stringParams.length === 0) {
      return baseURL;
    }
    const thereAreStringParams = stringParams.length > 0;
    const allParamsStr = thereAreStringParams
      ? `?${stringParams.join('&')}`
      : '';
    return `${baseURL}${allParamsStr}`;
  },

  getESDocument(indexName, docID, docSource, customIDProperty) {
    let docURL = `/es_data/get_es_document/${indexName}/${docID}`;
    const params = {};
    if (docSource != null) {
      params.source = docSource.join(',');
    }
    if (customIDProperty != null) {
      params.custom_id_property = customIDProperty;
    }

    docURL = methods.addQueryParamsToURL(docURL, params);
    return esProxyApiClient.get(docURL);
  },
};

export default methods;
